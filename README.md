# `envyfuzz`

This is the same general idea as the `nvfuzz` tool in `mesa/mesa`, with some
minor changes:

 - fast enough to search the full sm50 opcode space in a few seconds
 - easier-to-read output formatting
 - able to search multiple bit ranges in a single run
 - mode that flips every masked bit one-at-a-time instead of iterating over all possible values

This is enough to RE most instruction encodingings purely with `envyfuzz`, without
having to fight `ptxas` to emit the instruction you're interested in.

## Usage

```
Usage: envyfuzz [OPTIONS] --sm <SM> <BASE_INSTR> [RANGES]...

Arguments:
  <BASE_INSTR>
          Base instruction hex value

          Spaces and a '0x' prefix are allowed in the instruction value, for
          easier readability. These are ignored by the parser.

          The required length of depends on the shader model, since the
          different ISAs have different encoding lengths.

          SM5x: 8 bytes SM7x: 16 bytes

  [RANGES]...
          Bit ranges of the instruction to fuzz

          Ranges are formatted as 'LOW_BIT..HIGH_BIT', which includes all bits
          LOW_BIT <= bit < HIGH_BIT. Alternatively, single-bit ranges can be
          written as just 'BIT'. Ranges must be nonempty, and must not extend
          beyond the bounds of a single instruction.

Options:
  -v, --verbose
          Enable verbose output

          This will print all the nvdisasm output to stderr verbatim, in
          addition to the normal processed output.

  -q, --quiet
          Enable quiet output

          This hides all instructions that resulted in an error from nvdisasm.

      --nvdisasm <NVDISASM>
          Path to the nvdisasm executable

          It is also valid to pass the executable name, if it is in PATH.

          [env: ENVYFUZZ_NVDISASM=]
          [default: nvdisasm]

  -b, --batch-size <BATCH_SIZE>
          Size of instruction batches to pass to nvdisasm

          Larger values make fuzzer runs go *much* faster, but it's sometimes
          useful to set '--batch-size 1' if envyfuzz is having trouble parsing
          the nvdisasm output.

          [default: 4096]

  -s, --sm <SM>
          GPU architecture target

          Supported architectures are SM7<x> and SM5<x>.

          [env: ENVYFUZZ_SM=]

  -f, --flip
          Flip each bit in the specified ranges individually, instead of
          iterating over all possible values.

          This can be useful for searching the whole space of an instruction
          to find field boundaries.

  -h, --help
          Print help (see a summary with '-h')
```
