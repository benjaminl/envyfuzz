use std::{
    collections::HashSet,
    ffi::{OsStr, OsString},
    fmt::Write as WriteFmt,
    io::{self, Write},
    process::{self, Command},
    str,
    str::FromStr,
};

use clap::{CommandFactory, Parser, ValueEnum};
use derive_more::{Display, Error};
use tempfile::NamedTempFile;
use yansi::{Color, Paint};

#[derive(Eq, PartialEq, Copy, Clone, Display, Debug)]
enum Arch {
    #[display(fmt = "SM5{_0}")]
    Sm5x(u8),
    #[display(fmt = "SM7{_0}")]
    Sm7x(u8),
}

/// # Invariants
///
/// Range must not be empty, so `high > low`. This is enforced on construction.
///
/// Range must not extend past bit 128.
#[derive(Copy, Clone, Display, Debug)]
#[display(fmt = "{_0}..{_1}")]
struct BitRange(u8, u8);

#[derive(Parser, Debug)]
struct Cli {
    /// Enable verbose output
    ///
    /// This will print all the nvdisasm output to stderr verbatim, in addition
    /// to the normal processed output.
    #[arg(short, long)]
    verbose: bool,

    /// Enable quiet output
    ///
    /// This hides all instructions that resulted in an error from nvdisasm.
    #[arg(short, long)]
    quiet: bool,

    /// Path to the nvdisasm executable
    ///
    /// It is also valid to pass the executable name, if it is in PATH.
    #[arg(long, env = "ENVYFUZZ_NVDISASM", default_value = "nvdisasm")]
    nvdisasm: OsString,

    /// Size of instruction batches to pass to nvdisasm
    ///
    /// Larger values make fuzzer runs go *much* faster, but it's sometimes
    /// useful to set '--batch-size 1' if envyfuzz is having trouble parsing
    /// the nvdisasm output.
    #[arg(short, long, default_value = "4096")]
    batch_size: usize,

    /// GPU architecture target
    ///
    /// Supported architectures are SM7<x> and SM5<x>.
    #[arg(short, long, env = "ENVYFUZZ_SM")]
    sm: Arch,

    /// Flip each bit in the specified ranges individually, instead of
    /// iterating over all possible values.
    ///
    /// This can be useful for searching the whole space of an instruction to
    /// find field boundaries.
    #[arg(short, long)]
    flip: bool,

    /// Base instruction hex value
    ///
    /// Spaces and a '0x' prefix are allowed in the instruction value, for
    /// easier readability. These are ignored by the parser.
    ///
    /// The required length of depends on the shader model, since the different
    /// ISAs have different encoding lengths.
    ///
    /// SM5x: 8 bytes
    /// SM7x: 16 bytes
    base_instr: String,

    /// Bit ranges of the instruction to fuzz
    ///
    /// Ranges are formatted as 'LOW_BIT..HIGH_BIT', which includes all bits
    /// LOW_BIT <= bit < HIGH_BIT. Alternatively, single-bit ranges can be
    /// written as just 'BIT'. Ranges must be nonempty, and must not extend
    /// beyond the bounds of a single instruction.
    ranges: Vec<BitRange>,
}

#[derive(Error, Display, Debug)]
#[display(fmt = "invalid arch")]
struct ParseArchError;

impl FromStr for Arch {
    type Err = ParseArchError;

    fn from_str(s: &str) -> Result<Arch, ParseArchError> {
        let suffix = s
            .strip_prefix("SM")
            .or(s.strip_prefix("sm"))
            .ok_or(ParseArchError)?;
        let mut chars = suffix.chars();
        let digit1 = chars.next().ok_or(ParseArchError)?;
        let digit2 = chars.next().ok_or(ParseArchError)?;
        if chars.next().is_some() {
            return Err(ParseArchError);
        };
        let digit1 = digit1.to_digit(10).ok_or(ParseArchError)?;
        let digit2 = digit2.to_digit(10).ok_or(ParseArchError)?;
        // Never panics because all base-10 digits are in-range
        let digit1 = u8::try_from(digit1).unwrap();
        let digit2 = u8::try_from(digit2).unwrap();
        match (digit1, digit2) {
            (5, x) => Ok(Arch::Sm5x(x)),
            (7, x) => Ok(Arch::Sm7x(x)),
            _ => Err(ParseArchError),
        }
    }
}

impl Arch {
    /// Returns the size of an instruction for this arch in bytes
    fn encoding_size(self) -> usize {
        match self {
            Arch::Sm5x(_) => 8,
            Arch::Sm7x(_) => 16,
        }
    }
}

#[derive(Error, Display, Debug)]
#[display(fmt = "couldn't parse bit range")]
struct ParseBitRangeError;

impl FromStr for BitRange {
    type Err = ParseBitRangeError;

    fn from_str(s: &str) -> Result<BitRange, ParseBitRangeError> {
        let (low, high) = if let Some((low, high)) = s.split_once("..") {
            // Multi-bit range
            let low = low.parse().map_err(|_| ParseBitRangeError)?;
            let high = high.parse().map_err(|_| ParseBitRangeError)?;
            (low, high)
        } else {
            // Single-bit range
            let bit = s.parse().map_err(|_| ParseBitRangeError)?;
            (bit, bit + 1)
        };
        if high <= low || high > 128 || low > 128 {
            return Err(ParseBitRangeError);
        }
        Ok(BitRange(low, high))
    }
}

impl BitRange {
    fn mask(self) -> u128 {
        match 1u128.checked_shl((self.1 - self.0).into()) {
            Some(x) => (x - 1) << self.0,
            None => !0 << self.0,
        }
    }

    fn get(self, value: u128) -> u128 {
        (value & self.mask()) >> self.0
    }
}

#[test]
fn mask_start() {
    assert_eq!(BitRange(0, 10).mask(), 0b1111111111);
}
#[test]
fn mask_middle() {
    assert_eq!(BitRange(10, 20).mask(), 0b1111111111 << 10);
}
#[test]
fn mask_end() {
    assert_eq!(BitRange(118, 128).mask(), 0b1111111111 << 118);
}
#[test]
fn mask_full() {
    assert_eq!(BitRange(0, 128).mask(), !0);
}

#[derive(Error, Display, Debug)]
#[display(fmt = "couldn't parse instruction")]
struct ParseInstrError;

/// On SM75, the upper 64 bits are unset
#[derive(Display, Copy, Clone, Debug)]
#[display(fmt = "{:0digits$x}", value, "digits = sm.encoding_size() * 2")]
struct Instr {
    value: u128,
    sm: Arch,
}

impl Instr {
    fn parse(sm: Arch, s: &str) -> Result<Instr, ParseInstrError> {
        let s = s.replace("0x", "").replace(' ', "");
        if s.len() != sm.encoding_size() * 2 {
            return Err(ParseInstrError);
        }
        let value =
            u128::from_str_radix(&s, 16).map_err(|_| ParseInstrError)?;
        Ok(Instr {
            value,
            sm,
        })
    }

    /// Write the display-representation of the instruction to stout, with
    /// nibbles that are included in the mask colored purple, and nibbles
    /// that are not included colored blue.
    fn display_masked(self, mask: u128) {
        for nibble in (0..self.sm.encoding_size() * 2).rev() {
            let nibble_mask = 0xF << (nibble * 4);
            let value = (self.value & nibble_mask) >> (nibble * 4);
            // Won't panic, since value <= 0xf
            let digit =
                char::from_digit(value.try_into().unwrap(), 16).unwrap();
            let masked = mask & nibble_mask != 0;
            let color = if masked {
                Color::Magenta
            } else {
                Color::Blue
            };
            print!("{}", Paint::new(digit).fg(color));

            // Space between each dword
            if nibble % 8 == 0 {
                print!(" ");
            }
        }
    }

    fn encode(self, data: &mut Vec<u8>) {
        data.extend(&self.value.to_le_bytes()[..self.sm.encoding_size()]);
    }
}

#[derive(Error, Display, Debug)]
enum DisassembleError {
    #[display(fmt = "failed to create temp file: {_0}")]
    CreateTempFile(io::Error),
    #[display(fmt = "failed to run nvdisasm: {_0}")]
    StartNvdisasm(io::Error),
    #[display(
        fmt = "nvdisasm returned failed to parse some instructions: {_0:?}"
    )]
    InvalidInstrs(#[error(not(source))] HashSet<usize>),
    #[display(fmt = "nvdisasm returned error: {_0}")]
    NvdisasmError(#[error(not(source))] String),
    #[display(fmt = "failed to parse nvdisasm output:\n{_0}")]
    Parse(#[error(not(source))] String),
}

impl DisassembleError {
    /// Returns true if the error is "fatal", meaning that future calls to
    /// `disassemble` are unlikely to succeed, even given different instrs.
    fn fatal(&self) -> bool {
        match self {
            DisassembleError::CreateTempFile(_) => true,
            DisassembleError::StartNvdisasm(_) => true,
            DisassembleError::InvalidInstrs(_) => false,
            DisassembleError::NvdisasmError(_) => false,
            DisassembleError::Parse(_) => false,
        }
    }
}

fn parse_nvdisasm_output(
    sm: Arch,
    instr_count: usize,
    output: &str,
) -> Option<Vec<String>> {
    (0..instr_count)
        .map(|i| {
            let line_number = match sm {
                // each instruction takes one line
                // first line is header
                // sched line occurs every three instructions
                Arch::Sm5x(_) => i + 1 + (1 + i / 3),
                // each instruction takes two lines
                // first line is header
                Arch::Sm7x(_) => i * 2 + 1,
            };
            let line = output.lines().nth(line_number)?;
            // Cut off the indentation and position comment (but not predicate)
            let example_prefix = "        /*0008*/              ";
            let prefix_length = example_prefix.len();
            let instr_disasm = &line.get(prefix_length..)?;
            // Cut off the trailing comment
            let instr_disasm = match instr_disasm.split_once(';') {
                Some((instr_disasm, _)) => instr_disasm.trim_end(),
                None => {
                    // On SM75, some instrs output blank lines in the
                    // disassembly. these still have the
                    // trailing hex comment, but don't have
                    // the position comment or the actual instruction;
                    let before_comment = instr_disasm.split_once("/*")?.0;
                    if before_comment.chars().all(|c| c.is_whitespace()) {
                        ""
                    } else {
                        return None;
                    }
                }
            };
            Some(instr_disasm.to_owned())
        })
        .collect()
}

fn parse_nvdisasm_errors(sm: Arch, stderr: &str) -> Option<HashSet<usize>> {
    let failed_attrs = stderr
        .lines()
        .filter_map(|line| {
            if !line.starts_with("nvdisasm error   : ") {
                return None;
            }
            let address_str = line.split_once(" at address 0x")?.1;
            let address = u32::from_str_radix(address_str, 16).ok()?;
            let instr_index = match sm {
                Arch::Sm5x(_) => {
                    // each instr is 8 bytes
                    let index = address / 8;
                    // skip the sched every three instrs
                    index - (index / 4 + 1)
                }
                // each instr is 16 bytes
                Arch::Sm7x(_) => address / 16,
            };
            Some(usize::try_from(instr_index).unwrap())
        })
        .collect::<HashSet<_>>();

    if failed_attrs.is_empty() {
        None
    } else {
        Some(failed_attrs)
    }
}

fn disassemble(
    nvdisasm: &OsStr,
    verbose: bool,
    sm: Arch,
    instrs: impl Iterator<Item = Instr>,
) -> Result<Vec<String>, DisassembleError> {
    let mut data = Vec::new();

    let mut instr_count = 0;
    for instr in instrs {
        assert_eq!(instr.sm, sm);
        if let Arch::Sm5x(_) = sm {
            if instr_count % 3 == 0 {
                let sched = Instr {
                    value: 0x7E0 | 0x7E0 << 21 | 0x7E0 << 42,
                    sm,
                };
                sched.encode(&mut data);
            }
        }
        instr.encode(&mut data);
        instr_count += 1;
    }
    if let Arch::Sm5x(_) = sm {
        // NOP padding, for alignmentment with sched
        let padding = (3 - instr_count % 3) % 3;
        for _ in 0..padding {
            let nop = Instr {
                value: 0x50B0000000070F00,
                sm,
            };
            nop.encode(&mut data);
        }
    }

    let mut tempfile = NamedTempFile::with_prefix("envyfuzz.")
        .map_err(DisassembleError::CreateTempFile)?;
    tempfile.write_all(&data).map_err(DisassembleError::CreateTempFile)?;

    let output = Command::new(nvdisasm)
        .arg("--binary")
        .arg(sm.to_string())
        // for easier debugging when the parser doesn't work
        .arg("--print-instruction-encoding")
        .arg(tempfile.path())
        .output()
        .map_err(DisassembleError::StartNvdisasm)?;

    if verbose {
        io::stderr().write_all(&output.stderr).unwrap();
        io::stderr().write_all(&output.stdout).unwrap();
    }

    if !output.status.success() {
        if let Ok(stderr) = str::from_utf8(&output.stderr) {
            if let Some(failed_instrs) = parse_nvdisasm_errors(sm, stderr) {
                return Err(DisassembleError::InvalidInstrs(failed_instrs));
            }
        }
        let stderr = String::from_utf8_lossy(&output.stderr).into_owned();
        return Err(DisassembleError::NvdisasmError(stderr));
    }

    let stdout = str::from_utf8(&output.stdout).map_err(|_| {
        DisassembleError::Parse(
            String::from_utf8_lossy(&output.stdout).into_owned(),
        )
    })?;
    let instr_disasm = parse_nvdisasm_output(sm, instr_count, stdout)
        .ok_or(DisassembleError::Parse(stdout.to_owned()))?;
    Ok(instr_disasm)
}

struct TestCase {
    instr: Instr,
    /// Mask of bits that may have been modified from the base instruction.
    ///
    /// These will be highlighted in the output.
    modified_mask: u128,
    comment: String,
}

enum TestCasesKind {
    FlipBits,
    AllValues,
}

struct TestCases {
    kind: TestCasesKind,
    value: u128,
    ranges: Vec<BitRange>,
    mask: u128,
    base: Instr,
    done: bool,
}

impl TestCases {
    fn new(
        kind: TestCasesKind,
        ranges: Vec<BitRange>,
        base: Instr,
    ) -> TestCases {
        TestCases {
            value: match kind {
                TestCasesKind::FlipBits => 1,
                TestCasesKind::AllValues => 0,
            },
            kind,
            mask: ranges.iter().fold(0, |mask, range| mask | range.mask()),
            ranges,
            base,
            done: false,
        }
    }
}

impl Iterator for TestCases {
    type Item = TestCase;

    fn next(&mut self) -> Option<TestCase> {
        if self.done {
            return None;
        }

        match self.kind {
            TestCasesKind::FlipBits => {
                // Skip bits that are not set in the mask
                while self.mask & self.value == 0 {
                    self.value <<= 1;
                    if self.value == 0 {
                        self.done = true;
                        return None;
                    }
                }

                let action = if self.base.value & self.value != 0 {
                    "clr"
                } else {
                    "set"
                };
                let bit = self.value.trailing_zeros();

                let case = TestCase {
                    instr: Instr {
                        value: self.base.value ^ self.value,
                        ..self.base
                    },
                    modified_mask: self.value,
                    comment: format!("{action} {bit:3} : "),
                };

                self.value <<= 1;
                if self.value == 0 {
                    self.done = true;
                }

                Some(case)
            }
            TestCasesKind::AllValues => {
                let mut comment = String::new();
                for range in &self.ranges {
                    let max_digits =
                        usize::from((range.1 - range.0 - 1) / 4 + 1);
                    write!(
                        comment,
                        "{range}={:0max_digits$x} : ",
                        range.get(self.value)
                    )
                    .unwrap();
                }

                let case = TestCase {
                    instr: Instr {
                        value: (self.base.value & !self.mask) | self.value,
                        ..self.base
                    },
                    modified_mask: self.mask,
                    comment,
                };

                match (self.value | !self.mask).checked_add(1) {
                    Some(x) => self.value = x & self.mask,
                    None => self.done = true,
                }

                Some(case)
            }
        }
    }
}

fn main() {
    let cli = Cli::parse();

    let Ok(base_instr) = Instr::parse(cli.sm, &cli.base_instr) else {
        use clap::{
            error::{ContextKind, ContextValue, ErrorKind},
            Error,
        };
        let cmd = Cli::command();
        let mut error = Error::new(ErrorKind::ValueValidation).with_cmd(&cmd);
        error.insert(
            ContextKind::InvalidArg,
            ContextValue::String("BASE_INSTR".to_string()),
        );
        error.insert(
            ContextKind::InvalidValue,
            ContextValue::String(cli.base_instr),
        );
        error.exit();
    };

    let cases_kind = if cli.flip {
        TestCasesKind::FlipBits
    } else {
        TestCasesKind::AllValues
    };
    let mut cases = TestCases::new(cases_kind, cli.ranges, base_instr);

    let mut done = false;
    let mut batch = Vec::new();
    while !done {
        batch.clear();
        for _ in 0..cli.batch_size {
            match cases.next() {
                Some(case) => batch.push(case),
                None => {
                    done = true;
                    break;
                }
            }
        }
        if batch.is_empty() {
            break;
        }

        let results = match disassemble(
            &cli.nvdisasm,
            cli.verbose,
            cli.sm,
            batch.iter().map(|case| case.instr),
        ) {
            Ok(results) => results.into_iter().map(Some).collect(),
            Err(DisassembleError::InvalidInstrs(failed)) => {
                // Try again with all the instrs that didn't fail
                let mut results = vec![None; batch.len()];
                let retry_indices = (0..batch.len())
                    .filter(|i| !failed.contains(i))
                    .collect::<Vec<_>>();
                if !retry_indices.is_empty() {
                    let retry_instrs =
                        retry_indices.iter().map(|&i| batch[i].instr);
                    let retry_results = disassemble(
                        &cli.nvdisasm,
                        cli.verbose,
                        cli.sm,
                        retry_instrs,
                    );
                    let retry_results = match retry_results {
                        Ok(retry_results) => retry_results,
                        Err(e) => {
                            eprintln!("{}", Paint::red(&e));
                            if e.fatal() {
                                process::exit(1);
                            }
                            continue;
                        }
                    };
                    for (&i, disasm) in retry_indices.iter().zip(retry_results)
                    {
                        results[i] = Some(disasm);
                    }
                }
                results
            }
            Err(e) => {
                eprintln!("{}", Paint::red(&e));
                if e.fatal() {
                    process::exit(1);
                }
                continue;
            }
        };

        for (case, result) in batch.iter().zip(results) {
            let result = match &result {
                Some(disasm) => Paint::green(&disasm[..]),
                None => {
                    if cli.quiet {
                        continue;
                    } else {
                        Paint::red("ERROR")
                    }
                }
            };
            print!("{}", case.comment);
            case.instr.display_masked(case.modified_mask);
            println!(": {}", result);
        }
    }
}
